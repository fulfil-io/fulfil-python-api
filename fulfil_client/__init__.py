# -*- coding: utf-8 -*-

__author__ = 'Fulfil.IO Inc.'
__email__ = 'hello@fulfil.io'
__version__ = '0.4.0'


from .client import Client, Model, ServerError
